---
layout: page
title: MNT Reform apt repository
permalink: /repo/
---

<h3>Table of Contents</h3>

* This will become a table of contents (this text will be scrapped).
{:toc}

## Introduction

If you are an experienced user and know what you are doing, then you might just
need this: <a href="/reform_bookworm.sources">reform_bookworm.sources</a>. For
more information on how to convert an existing installation from [the official
MNT system image](https://source.mnt.re/reform/reform-system-image/-/pipelines)
to Bookworm just read on.

These instructions are not relevant if you installed your system using the
system image or Debian installer image from reform.debian.net. Those already
ship with the correct apt sources for Bookworm packages from this repository.

In contrast to the [apt repository](https://mntre.com/reform-debian-repo)
provided by MNT Research, this repository provides its content based on Debian
stable (Bookworm) instead of unstable for a more reliable computing experience.
The the packages in this repository are signed with a GPG key that is part of
the Debian Developer keyring shipped by the `debian-keyring` package on your
system and thus there exists a trust path from your existing installation to
this repository.

The MNT Reform system image from source.mnt.re uses Debian `unstable`. If you
haven't upgraded your system since the release of Debian `bookworm`, then you
can easily turn your Debian `unstable` into a Debian `stable` (codename
`bookworm`) system. The advantage of doing so is, that you then no longer have
to worry about running `apt upgrade` breaking your running system due to some
RC bug. Doing the switch from `unstable` to `bookworm` involves changing two
aspects of your `apt` configuration and the final update and upgrade.

Please note, that you can *not* convert an installation of Debian `unstable` to
`bookworm` if it is younger than around April 2023. If your Debian `unstable`
installation is younger than that, converting to `bookworm` would require
downgrading package versions and downgrades are not supported by Debian.

## Step 1: Adjust apt sources

The default `/etc/apt/sources.list` on a Reform system image looks like this:

    deb http://deb.debian.org/debian unstable main
    deb [arch=arm64 trusted=yes] https://mntre.com/reform-debian-repo reform main

The first line configures Debian `unstable` as the source for the packages you
install and upgrade to. The second line configures the official MNT mirror. To
switch to bookworm and the package repository provided by reform.debian.net,
you have to

 1. replace `unstable` with `bookworm` in the first line,
 2. add stable update and security mirrors and
 3. replace the MNT mirror with the reform.debian.net mirror.

Your resulting `/etc/apt/sources.list` should look like this:

    deb http://deb.debian.org/debian bookworm main
    deb http://deb.debian.org/debian bookworm-updates main
    deb http://security.debian.org/debian-security bookworm-security main

You can choose between three different ways to enable the reform.debian.net
mirror:

### Method A: Using `extrepo`

Install the package `extrepo` and then run:

    extrepo enable reform.debian.net

This will create `/etc/apt/sources.list.d/extrepo_reform.debian.net.sources`
with the correct contents automatically using GPG signed repository
descriptions from the central extrepo mirror. If you rather trust the
locally installed `extrepo-data` package you can also run:

    extrepo enable --offlinedata reform.debian.net

### Method B: Manually downloading deb822 sources file

You can also manually download `reform_bookworm.sources` and place it in
`/etc/apt/sources.list.d` like this:

    sudo env --chdir=/etc/apt/sources.list.d wget https://reform.debian.net/reform_bookworm.sources

If you go this route instead of using `extrepo`, see below for a method to
verify that the GPG key attached to your downloaded `reform_bookworm.sources`
is indeed the one that is also present in your
`/usr/share/keyrings/debian-keyring.gpg`.

### Method C: Manually adding one-line entry

If you prefer to use the one-line format of `/etc/apt/sources.list` instead of
the deb822 format of `reform_bookworm.sources`, you have to download the key to
a custom location (for example to `/etc/apt/keyrings/reform.debian.net.asc`)
and add this line to your `/etc/apt/sources.list`:

    deb [arch=arm64 signed-by=/etc/apt/keyrings/reform.debian.net.asc] https://reform.debian.net/debian bookworm main

See below for a method to extract the right key from
`/usr/share/keyrings/debian-keyring.gpg` and store it in
`/etc/apt/keyrings/reform.debian.net.asc` or another location of your choosing
and referenced by the `signed-by` option in your `one-line` apt sources entry.

## Step 2: Adjust apt preferences

The original MNT Reform system image contains a preference file which
prioritizes packages from the mntre.com repository over packages coming from
elsewhere. You need to do the same with the reform.debian.net repository.
Without setting up this pinning, you run the risk of accidentally upgrading to
package versions from debian.org for those packages patched by
reform.debian.net in the small time window before reform.debian.net managed to
rebuild new package versions with the reform-specfic patches on top. To this
end, comment out or remove the existing content of
`/etc/apt/preferences.d/reform.pref` and add this:

    Package: *
    Pin: origin "reform.debian.net"
    Pin-Priority: 999

## Step 3: Upgrading

You can now resynchronize the package index files from their sources by
running:

    sudo apt update

You can verify that your apt sources are set up correctly by ensuring that the
output of the following command does include `https://reform.debian.net/debian`
and does NOT include `https://mntre.com/reform-debian-repo`:

    apt-get indextargets --format '$(SITE)' 'Created-By: Packages' | sort -u

You can verify that apt pinning via apt preferences worked as expected by
verifying the apt pin value of `linux-image-arm64` was indeed set to 999 and
comes from reform.debian.net:

    apt-cache policy linux-image-arm64 | grep "999 https://reform.debian.net"

If everything checks out, you can run these commands to finish the conversion:

    sudo apt upgrade --without-new-pkgs
    sudo apt full-upgrade

## GPG signature

The repository is signed by a subkey of the following key:

    F83356BBE112B7462A41552F7D5D8C60CF4D3EB4 josch@debian.org

This key is part of the keyring shipped by the `debian-keyring` package
2023.07.22 or later in `/usr/share/keyrings/debian-keyring.gpg`. To extract
the key from the keyring you can run:

    gpg --no-options --no-default-keyring --export --armour \
      --keyring /usr/share/keyrings/debian-keyring.gpg \
      --export-options export-minimal,export-clean \
      --export-filter keep-uid="uid = Johannes Schauer Marin Rodrigues <josch@debian.org>" \
      --export-filter drop-subkey="fpr <> 3AC6EB840FA5CE3FF31BAD80EF93221F8A44FEB2" \
      F83356BBE112B7462A41552F7D5D8C60CF4D3EB4

Depending on which method you chose to set up your apt sources, this key should
either:

 * Method A (extrepo): the key is referenced by `/etc/apt/sources.list.d/extrepo_reform.debian.net.sources` and likely stored in /var/lib/extrepo/keys/reform.debian.net.asc
 * Method B (deb822): the key is stored in the `Signed-By` field in `/etc/apt/sources.list.d/reform_bookworm.sources`
 * Method C (one-line): the key is referenced by the `signed-by` option and stored in a location of your choosing (like `/etc/apt/keyrings/reform.debian.net.asc`)

## Backports

The Linux kernel from bookworm-backports is provided for support for more
features (suspend on imx8mq) and more platforms (a311d). To enable the
backports repository, follow method B or C from above but replace bookworm with
bookworm-backports. For example for method B, do:

    sudo env --chdir=/etc/apt/sources.list.d wget https://reform.debian.net/reform_bookworm-backports.sources

Or, for method C, add the following to your `sources.list`:

    deb [arch=arm64 signed-by=/etc/apt/keyrings/reform.debian.net.asc] https://reform.debian.net/debian bookworm-backports main

And then install the correct kernel using:

    sudo apt install linux-image-arm64/bookworm-backports

## Source

The packages are generated by the following script:
[https://salsa.debian.org/reform-team/reform-debian-packages](https://salsa.debian.org/reform-team/reform-system-image/)
