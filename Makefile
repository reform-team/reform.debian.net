.PHONY: all
all: _site/index.html

_site/index.html:
	jekyll build

.PHONY: serve
serve:
	jekyll serve
