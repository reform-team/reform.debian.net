---
layout: page
permalink: /
---

<img src="/mnt-reform-single-1.jpg" style="float:right;margin:5px" />

<h1>MNT Reform Debian disk images and package repository</h1>

<div class="important" style='display:table'><p><strong>This website is NOT an official resource by
MNT Research GmbH</strong></p><p>The content on these pages is independently
provided by the Reform Debian community. The official images can be found at <a
href="https://mnt.re/system-image">https://mnt.re/system-image</a> and the
official repository at <a
href="https://mntre.com/reform-debian-repo/">https://mntre.com/reform-debian-repo/</a>.</p></div>

This website provides bootable system images, a patched Debian installer, as
well as an apt repository for the <a
href="https://shop.mntre.com/products/mnt-reform">MNT Reform</a>, the <a
href="https://www.crowdsupply.com/mnt/pocket-reform">MNT Pocket Reform</a> and
the Reform Next open source hardware family of computers. To learn more about
MNT Reform computers, visit <a href="https://mntre.com">mntre.com</a>, get
involved via <a href="https://community.mnt.re/">the forum</a> or via IRC in
#mnt-reform on irc.libera.chat:6697 or read the source code at <a
href="https://source.mnt.re/reform">source.mnt.re/reform</a> and <a
href="https://salsa.debian.org/reform-team">salsa.debian.org/reform-team</a>.

The bootable system image will let you try out Debian immediately. Just flash
the provided system image to an SD-card (or USB flash drive) and turn on your
Reform with the SD-Card (or USB stick) inserted. You can install Debian to eMMC
or NVMe by using the `reform-migrate` and `reform-setup-encrypted-nvme` scripts
on that system. If you prefer installing Debian using the Debian installer you
can flash the d-i image to a USB flash drive and install Debian to your Reform
in the same way as you would for other machines. If you already have Debian
installed and would like to switch to using Debian Bookworm, you can set up
your system to use the reform.debian.net apt repository.

<div style="float:left;width:14em;text-align:center;margin:1em">
<a href="{{ 'images' | relative_url }}">
<img src="Pics/google-noto-1f4be-floppy-disk.svg" width="80%"><br>
Flash a bootable Debian system image to an SD-Card</a>
</div>
<div style="float:left;width:14em;text-align:center;margin:1em">
<a href="{{ 'd-i' | relative_url }}">
<img src="Pics/google-noto-1f97e-hiking-boot.svg" width="80%"><br>
Install Debian using the traditional Debian Installer</a>
</div>
<div style="float:left;width:14em;text-align:center;margin:1em">
<a href="{{ 'repo' | relative_url }}">
<img src="Pics/google-noto-1f4e6-package.svg" width="80%"><br>
Add the Debian apt package repository to your existing installation</a>
</div>

<br style="clear:both">

In contrast to the [Debian system
images](https://source.mnt.re/reform/reform-system-image/-/pipelines) and [apt
repository](https://mntre.com/reform-debian-repo) provided by MNT Research,
this page provides its content based on Debian stable (Bookworm) instead of
unstable for a more reliable computing experience. The disk images as well as
the packages in the apt repository are signed with a GPG key that is part of
the Debian Developer keyring shipped by the `debian-keyring` package on your
system.

The contents on these pages are regenerated daily. You can find the last time
that the this page and the content behind it got generated at the footer of
every page.
