---
layout: page
title: Debian Installer for MNT Reform
permalink: /d-i/
---

These disk images contain Debian Installer netboot images similar to the
[official SD-card
images](http://ftp.debian.org/debian/dists/bookworm/main/installer-arm64/current/images/netboot/SD-card-images/)
but prepared with all changes necessary to allow installation on the Reform.
Due to non-free firmware blobs ([see below](#non-free)) these custom images are
likely going to remain necessary. Alternatively, you can use [full system
images](/images) to quickly try out a desktop installation on your Reform
without making changes to your eMMC or NVMe.

<h3>Table of Contents</h3>

* This will become a table of contents (this text will be scrapped).
{:toc}

## Manual partitioning for `/boot`

<!--
[30;44m                                                                                
  [30;47m┌────────────────────────┤ [31;47m[!!] Partition disks[30;47m ├─────────────────────────┐[30;44m   
  [30;47m│                                                                         │[30;40m [30;44m  
  [30;47m│ This is an overview of your currently configured partitions and mount   │[30;40m [30;44m  
  [30;47m│ points. Select a partition to modify its settings (file system, mount   │[30;40m [30;44m  
  [30;47m│ point, etc.), a free space to create partitions, or a device to         │[30;40m [30;44m  
  [30;47m│ initialize its partition table.                                         │[30;40m [30;44m  
  [30;47m│                                                                         │[30;40m [30;44m  
  [30;47m│            [37;41mMMC/SD card #1 (mmcblk0) - 15.7 GB MMC TB2916 [30;47m  -            │[30;40m [30;44m  
  [30;47m│            >           4.2 MB       FREE SPACE             ▒            │[30;40m [30;44m  
  [30;47m│            >     #1  511.7 MB    f  ext4          /boot    ▒            │[30;40m [30;44m  
  [30;47m│            >          15.1 GB       FREE SPACE             0            │[30;40m [30;44m  
  [30;47m│            /dev/nvme0n1 - 1.0 TB WDC WDS100T2B0C-00PXH0    ▒            │[30;40m [30;44m  
  [30;47m│            >           1.0 MB       FREE SPACE             ▒            │[30;40m [30;44m  
  [30;47m│            >     #1    8.0 GB    f  swap          swap     ▒            │[30;40m [30;44m  
  [30;47m│            >     #2    120 GB    f  ext4          /        ▒            │[30;40m [30;44m  
  [30;47m│                                                            .            │[30;40m [30;44m  
  [30;47m│                                                                         │[30;40m [30;44m  
  [30;47m│     <Go Back>                                                           │[30;40m [30;44m  
  [30;47m│                                                                         │[30;40m [30;44m  
  [30;47m└─────────────────────────────────────────────────────────────────────────┘[30;40m [30;44m  
   [30;40m                                                                           [30;44m  
[97;44;1m<F1> for help; <Tab> moves; <Space> selects; <Enter> activates buttons          
-->

<div style="float:right;margin:5px;width=800px">
<pre style='font-weight:normal;line-height:normal;color:#000;white-space:pre;word-wrap: break-word;overflow-wrap: break-word;'><span style="background-color: #0000aa;">                                                                                
  </span><span style="background-color:#aaaaaa;">┌────────────────────────┤ </span><span style="color: #aa0000;background-color:#aaaaaa;">[!!] Partition disks</span><span style="background-color:#aaaaaa;"> ├─────────────────────────┐</span><span style="color: #000000;background-color: #0000aa;">   
  </span><span style="background-color:#aaaaaa;">│                                                                         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│ This is an overview of your currently configured partitions and mount   │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│ points. Select a partition to modify its settings (file system, mount   │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│ point, etc.), a free space to create partitions, or a device to         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│ initialize its partition table.                                         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│                                                                         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            </span><span style="color: #aaaaaa;background-color:#aa0000;">MMC/SD card #1 (mmcblk0) - 15.7 GB MMC TB2916 </span><span style="background-color:#aaaaaa;">  -            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;           4.2 MB       FREE SPACE             ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;     #1  511.7 MB    f  ext4          /boot    ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;          15.1 GB       FREE SPACE             0            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            /dev/nvme0n1 - 1.0 TB WDC WDS100T2B0C-00PXH0    ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;           1.0 MB       FREE SPACE             ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;     #1    8.0 GB    f  swap          swap     ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│            &gt;     #2    120 GB    f  ext4          /        ▒            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│                                                            .            │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│                                                                         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│     &lt;Go Back&gt;                                                           │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">│                                                                         │</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
  </span><span style="background-color:#aaaaaa;">└─────────────────────────────────────────────────────────────────────────┘</span><span style="background-color:#000;"> </span><span style="color: #000000;background-color: #0000aa;">  
   </span><span style="background-color:#000;">                                                                           </span><span style="color: #000000;background-color: #0000aa;">  
</span><span style="color:#ffffff;background-color:#0000aa;font-weight:bold">&lt;F1&gt; for help; &lt;Tab&gt; moves; &lt;Space&gt; selects; &lt;Enter&gt; activates buttons          
</span></pre>
</div>

These images contain the custom kernel from the reform.d.n repos with all
required modules baked into the initramfs. Make sure to choose manual
partitioning and place the /boot partition where your SoM can read it:

| CPU Module                          | SD-card    | eMMC       | NVMe     |
|-------------------------------------|------------------------------------|
| Pocket Reform with BPI-CM4 Module   | ✅ mmcblk0 | ❌         | ✅ nvme0 |
| Pocket Reform with i.MX8MP Module   | ✅ mmcblk0 | ✅ mmcblk2 | ❌       |
| Reform 2                            | ✅ mmcblk1 | ✅ mmcblk0 | ❌       |
| Reform 2 with BPI-CM4 Module        | ✅ mmcblk0 | ❌         | ✅ nvme0 |
| Reform 2 with i.MX8MP Module        | ✅ mmcblk0 | ✅ mmcblk2 | ❌       |
| Reform 2 with LS1028A Module        | ✅ mmcblk0 | ❌         | ❌       |
| Reform 2 with RCORE RK3588 Module   | ✅ mmcblk1 | ❌         | ❌       |

## Reform-specific installation steps

At the end of the installation, a few additional steps are carried out:

 1. for i.MX8MQ: choose single or dual display configuration
 2. run `flash-kernel`
 3. run `update-initramfs -u`
 4. copy `flash.bin` into `/boot`
 5. choose whether to flash u-boot onto SD-card or eMMC (only valid choices will be displayed)
 6. run `reform-flash-uboot --offline`

## Download

You can obtain the machine name of your device (the first column) from the output of running `cat /proc/device-tree/model`.

| Machine name | Debian 12 Bookworm Stable | Debian 12 Bookworm Stable (backports kernel) |
|--------------|---------------------------|----------------------------------------------|
{% for machine in site.data.d-i -%}
| {{ machine.name }} | {% if machine.bookworm %}<a href='{{ machine.sysimage }}.img.xz'>{{ machine.sysimage }}.img.xz</a> ({{ machine.bookworm }}, <a href='{{ machine.sysimage }}.img.xz.sig'>GPG sig</a>){% else %}n.a.{% endif %} | <a href='{{ machine.sysimage }}-bpo.img.xz'>{{ machine.sysimage }}-bpo.img.xz</a> ({{ machine.bookworm-bpo }}, <a href='{{ machine.sysimage }}-bpo.img.xz.sig'>GPG sig</a>) |
{% endfor %}

## How to verify GPG signature

    gpgv --keyring /usr/share/keyrings/debian-keyring.gpg reform2-system.img.xz.sig /path/to/reform2-system.img.xz

## How to flash to SD-card or USB flash drive

The disk image contains the bootloader (U-Boot) as well as a partition table,
so it has to be written to the SD-card or USB stick directly instead of being
copied to a mounted filesystem or written onto an existing partition. As a
result, writing the image to your SD-card or USB stick will **destroy all the
data** that was on it before. Be very certain that the device you are writing
the image to is indeed the SD-card or flash drive you inserted and not any
other disk. The following command will print the device node of any disk you
attach to your machine while the command is running:

    udevadm monitor --udev --property --subsystem-match=block/disk | sed -ne 's/^DEVNAME=//p'

### Flash using `dd`

You can copy the d-i image using `dd`. Make sure to replace
`/path/to/reform2-d-i.img` by the path to the d-i image you downloaded and
`/dev/mmcblkXXX` by the block device belonging to your SD-card or USB flash
drive.

    xzcat /path/to/reform2-d-i.img.xz | sudo dd of=/dev/mmcblkXXX status=progress

### Flash using `bmaptool`

If you have the package `bmap-tools` installed, then instead of using `dd` you
can flash the image to your SD-card like this:

    sudo bmaptool copy https://reform.debian.net/d-i/reform-systemXXX.img.xz /dev/mmcblkXXX

The advantages of using `bmaptool` over using `dd` are:

 - will only copy blocks with data and skips empty blocks (around 30% less data to write)
 - verifies checksums of downloaded data to prevent corrupt images
 - safety checks to make sure that image is not written to mounted disks
 - able to directly download from http server without local temporary files
 - automatic decompression on-the-fly
 - verifies GPG signatures

## non-free blobs
{: #non-free }

These images contain non-free material in the form of DDR training blobs, ARM
trusted firmware blobs, WiFi/BT firmware and/or HDMI/eDP firmware. The non-free
blobs are loaded onto the hardware early-on during the boot process and do not
run on the main processor. Sometimes the blobs are optional. For example the
HDMI blobs are only necessary if you need HDMI output. If you have your U-Boot
on eMMC, then you do not need U-Boot on your SD-card and would thus be able to
have a DFSG-free SD-card image. If you care for that, simply zero-out the
respective range of bytes between the partition table and the first partition.
If you are booting from a USB flash drive instead of from an SD-card, the
U-Boot binary will not be used.

For more information about the differences between the modules see <a
href="https://mntre.com/modularity.html#table">https://mntre.com/modularity.html#table</a>.

| CPU Module | non-free blobs |
|------------|----------------|
| NXP i.MX8MQ (default)                   | Synopsys DDR4 training blob, Cadence HDMI blob |
| NXP i.MX8MPlus                          | Synopsys DDR4 training blob, WiFi/BT firmware |
| NXP Layerscape LS1028A                  | eDP display firmware blob |
| RCM4 with Banana Pi CM4 (Amlogic A311D) | ARM trusted firmware, WiFi firmware |
| RCM4 with Raspberry Pi CM4              | boot blob |

## Source

The images are generated by the following scripts: <a
href="https://salsa.debian.org/reform-team/reform-debian-installer/">https://salsa.debian.org/reform-team/reform-debian-installer</a>
