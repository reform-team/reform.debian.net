---
layout: page
title: MNT Reform system images
permalink: /images/
---

These disk images contain a bootable full Debian installation which can be
flashed to an SD-card or USB flash drive from which your Reform can then boot
directly into a full desktop installation. The image contains two partitions:
one for `/boot` and one for and `/`. U-Boot has been written to the correct
offset at the beginning of the image. Log in as `root` (empty password), change
the root password using `passwd` and create yourself a normal user using
adduser. The rescue system contains `sway` and `wayfire` for a graphical
desktops.

If you do not want to operate your Reform from the SD-card or USB stick (slow
and wears down the flash storage quickly) you can copy its contents to eMMC or
NVMe after having booted your Reform from the SD-card or USB drive. The scripts
`reform-flash-rescue`, `reform-migrate` and `reform-setup-encrypted-nvme` from
the `reform-tools` package help with that task. Alternatively, you can use <a
href="/d-i">the Debian Installer</a> to install Debian onto your Reform like
you would install Debian on any other platform.

<h3>Table of Contents</h3>

* This will become a table of contents (this text will be scrapped).
{:toc}

You can obtain the machine name of your device (the first column) from the output of running `cat /proc/device-tree/model`.

| Machine name | Debian 12 Bookworm Stable | Debian 12 Bookworm Stable (backports kernel) |
|--------------|---------------------------|----------------------------------------------|
{% for machine in site.data.images -%}
| {{ machine.name }} | {% if machine.bookworm %}<a href='{{ machine.sysimage }}.img.xz'>{{ machine.sysimage }}.img.xz</a> ({{ machine.bookworm }}, <a href='{{ machine.sysimage }}.img.xz.sig'>GPG sig</a>){% else %}n.a.{% endif %} | <a href='{{ machine.sysimage }}-bpo.img.xz'>{{ machine.sysimage }}-bpo.img.xz</a> ({{ machine.bookworm-bpo }}, <a href='{{ machine.sysimage }}-bpo.img.xz.sig'>GPG sig</a>) |
{% endfor %}

## How to verify GPG signature

    gpgv --keyring /usr/share/keyrings/debian-keyring.gpg reform2-system.img.xz.sig /path/to/reform2-system.img.xz

## How to flash to SD-card or USB flash drive

The disk image contains the bootloader (U-Boot) as well as a partition table,
so it has to be written to the SD-card or USB stick directly instead of being
copied to a mounted filesystem or written onto an existing partition. As a
result, writing the image to your SD-card or USB stick will **destroy all the
data** that was on it before. Be very certain that the device you are writing
the image to is indeed the SD-card or flash drive you inserted and not any
other disk. The following command will print the device node of any disk you
attach to your machine while the command is running:

    udevadm monitor --udev --property --subsystem-match=block/disk | sed -ne 's/^DEVNAME=//p'

### Flash using `dd`

You can copy the system image using `dd`. Make sure to replace
`/path/to/reform2-system.img` by the path to the system image you downloaded
and `/dev/mmcblkXXX` by the block device belonging to your SD-card or USB flash
drive.

    xzcat /path/to/reform2-system.img.xz | sudo dd of=/dev/mmcblkXXX status=progress

### Flash using `bmaptool`

If you have the package `bmap-tools` installed, then instead of using `dd` you
can flash the image to your SD-card like this:

    sudo bmaptool copy https://reform.debian.net/images/reform-systemXXX.img.xz /dev/mmcblkXXX

The advantages of using `bmaptool` over using `dd` are:

 - will only copy blocks with data and skips empty blocks (around 30% less data to write)
 - verifies checksums of downloaded data to prevent corrupt images
 - safety checks to make sure that image is not written to mounted disks
 - able to directly download from http server without local temporary files
 - automatic decompression on-the-fly
 - verifies GPG signatures

## non-free blobs

These images contain non-free material in the form of DDR training blobs, ARM
trusted firmware blobs, WiFi/BT firmware and/or HDMI/eDP firmware. The non-free
blobs are loaded onto the hardware early-on during the boot process and do not
run on the main processor. Sometimes the blobs are optional. For example the
HDMI blobs are only necessary if you need HDMI output. If you have your U-Boot
on eMMC, then you do not need U-Boot on your SD-card and would thus be able to
have a DFSG-free SD-card image. If you care for that, simply zero-out the
respective range of bytes between the partition table and the first partition.
If you are booting from a USB flash drive instead of from an SD-card, the
U-Boot binary will not be used.

For more information about the differences between the modules see <a
href="https://mntre.com/modularity.html#table">https://mntre.com/modularity.html#table</a>.

| CPU Module | non-free blobs |
|------------|----------------|
| NXP i.MX8MQ (default)                   | Synopsys DDR4 training blob, Cadence HDMI blob |
| NXP i.MX8MPlus                          | Synopsys DDR4 training blob, WiFi/BT firmware |
| NXP Layerscape LS1028A                  | eDP display firmware blob |
| RCM4 with Banana Pi CM4 (Amlogic A311D) | ARM trusted firmware, WiFi firmware |

## Source

The images are generated by the following scripts: <a
href="https://salsa.debian.org/reform-team/reform-system-image/">https://salsa.debian.org/reform-team/reform-system-image</a>
